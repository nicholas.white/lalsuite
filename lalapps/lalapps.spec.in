%define nightly @NIGHTLY_VERSION@%{nil}
%define _prefix /usr
%define _mandir %{_prefix}/share/man
%define _sysconfdir %{_prefix}/etc
%define _pkgpythondir %{_prefix}/lib64/python2.?/site-packages/@PACKAGE@
%define _pkgpyexecdir %{_libdir}/python2.?/site-packages/@PACKAGE@
%define release 1

%if "%{?nightly:%{nightly}}%{!?nightly:0}" == "%{nil}"
%undefine nightly
%endif

Name: @PACKAGE@
Version: @BASE_VERSION@
Release: %{?nightly:0.%{nightly}}%{!?nightly:%{release}}%{?dist}
Summary: LSC Algorithm Library Applications
License: GPLv2+
Group: LAL
Source: %{name}-%{version}%{?nightly:-%{nightly}}.tar.xz
URL: https://wiki.ligo.org/DASWG/LALSuite
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildRequires: cfitsio-devel
BuildRequires: fftw-devel
BuildRequires: gsl-devel
BuildRequires: less
BuildRequires: libframe-devel
BuildRequires: libmetaio-devel
BuildRequires: openmpi-devel
BuildRequires: lal-devel >= @MIN_LAL_VERSION@
BuildRequires: python2-lal >= @MIN_LAL_VERSION@
BuildRequires: lalframe-devel >= @MIN_LALFRAME_VERSION@
BuildRequires: python2-lalframe >= @MIN_LALFRAME_VERSION@
BuildRequires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@
BuildRequires: python2-lalmetaio >= @MIN_LALMETAIO_VERSION@
BuildRequires: lalsimulation-devel >= @MIN_LALSIMULATION_VERSION@
BuildRequires: python2-lalsimulation >= @MIN_LALSIMULATION_VERSION@
BuildRequires: lalburst-devel >= @MIN_LALBURST_VERSION@
BuildRequires: python2-lalburst >= @MIN_LALBURST_VERSION@
BuildRequires: lalinspiral-devel >= @MIN_LALINSPIRAL_VERSION@
BuildRequires: python2-lalinspiral >= @MIN_LALINSPIRAL_VERSION@
BuildRequires: lalstochastic-devel >= @MIN_LALSTOCHASTIC_VERSION@
BuildRequires: python2-lalstochastic >= @MIN_LALSTOCHASTIC_VERSION@
BuildRequires: lalpulsar-devel >= @MIN_LALPULSAR_VERSION@
BuildRequires: python2-lalpulsar >= @MIN_LALPULSAR_VERSION@
BuildRequires: lalinference-devel >= @MIN_LALINFERENCE_VERSION@
BuildRequires: python2-lalinference >= @MIN_LALINFERENCE_VERSION@
Requires: cfitsio
Requires: fftw
Requires: gsl
Requires: h5py
Requires: healpy
Requires: less
Requires: libframe
Requires: libmetaio
Requires: openmpi
Requires: openssh-clients
Requires: python
Requires: lal >= @MIN_LAL_VERSION@
Requires: python2-lal >= @MIN_LAL_VERSION@
Requires: lalframe >= @MIN_LALFRAME_VERSION@
Requires: python2-lalframe >= @MIN_LALFRAME_VERSION@
Requires: lalmetaio >= @MIN_LALMETAIO_VERSION@
Requires: python2-lalmetaio >= @MIN_LALMETAIO_VERSION@
Requires: lalsimulation  >= @MIN_LALSIMULATION_VERSION@
Requires: python2-lalsimulation >= @MIN_LALSIMULATION_VERSION@
Requires: lalburst >= @MIN_LALBURST_VERSION@
Requires: python2-lalburst >= @MIN_LALBURST_VERSION@
Requires: lalinspiral >= @MIN_LALINSPIRAL_VERSION@
Requires: python2-lalinspiral >= @MIN_LALINSPIRAL_VERSION@
Requires: lalstochastic >= @MIN_LALSTOCHASTIC_VERSION@
Requires: python2-lalstochastic >= @MIN_LALSTOCHASTIC_VERSION@
Requires: lalpulsar >= @MIN_LALPULSAR_VERSION@
Requires: python2-lalpulsar >= @MIN_LALPULSAR_VERSION@
Requires: lalinference >= @MIN_LALINFERENCE_VERSION@
Requires: python2-lalinference >= @MIN_LALINFERENCE_VERSION@
Requires: skyarea
Prefix: %{_prefix}

%description
The LSC Algorithm Library Applications for gravitational wave data analysis.
This package contains applications that are built on tools in the LSC
Algorithm Library.

%prep
%setup -q -n %{name}-%{version}%{?nightly:-%{nightly}}

%build
%configure --disable-gcc-flags --enable-cfitsio --enable-openmp --enable-mpi MPICC=/usr/lib64/openmpi/bin/mpicc MPICXX=/usr/lib64/openmpi/bin/mpicxx MPIFC=/usr/lib64/openmpi/bin/mpifc
%{__make} %{?_smp_mflags} V=1

%check
%{__make} %{?_smp_mflags} V=1 VERBOSE=1 check

%install
%make_install

%post
ldconfig

%postun
ldconfig

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}%{?nightly:-%{nightly}}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/lalapps/*
%{_mandir}/man1/*
%{_pkgpythondir}/*
%{_sysconfdir}/lalapps-user-env.*

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Tue Feb 07 2017 Adam Mercer <adam.mercer@ligo.org> 6.21.0-1
- O2 release

* Mon Sep 26 2016 Adam Mercer <adam.mercer@ligo.org> 6.20.0-1
- ER10 release

* Thu Jun 23 2016 Adam Mercer <adam.mercer@ligo.org> 6.19.0-1
- ER9 release

* Fri Mar 25 2016 Adam Mercer <adam.mercer@ligo.org> 6.18.0-1
- Pre O2 packaging test release
